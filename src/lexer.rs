use std::io::*;
use std::iter::*;
use std::str::Chars;
use std::process;

#[derive(Debug)]
pub enum Op {
    Plus,
    Minus,
    Times,
    Div,
}

#[derive(Debug)]
pub enum KeyWord {
    Graph,
    End,
}

#[derive(Debug)]
pub enum Token {
    Law(Op),
    Num(f32),
    KW(KeyWord),
    X,
    Y,
    Eq,
    OpenParen,
    CloseParen,
}

pub struct Lexer<R: Read> {
    lines: Lines<BufReader<R>>,
    t_vec: Vec<Token>,
}

impl<R: Read> Lexer<R> {
    pub fn lex(lines: Lines<BufReader<R>>) -> Result<Vec<Token>> {
        let mut lexer = Lexer {
            lines,
            t_vec: Vec::new(),
        };

        while let Some(Ok(line)) = lexer.lines.next() {
            lexer.lex_line(&mut line.chars().peekable())?;
        }

        Ok(lexer.t_vec)
    }

    fn lex_line(&mut self, line: &mut Peekable<Chars>) -> Result<()> {
        match line.peek() {
            Some('[') => {
                self.lex_kw(line);
            }
            Some('+') => {
                self.t_vec.push(Token::Law(Op::Plus));
                line.next();
                let _ = self.lex_line(line);
            }
            Some('-') => {
                self.t_vec.push(Token::Law(Op::Minus));
                line.next();
                let _ = self.lex_line(line);
            }
            Some('*') => {
                self.t_vec.push(Token::Law(Op::Times));
                line.next();
                let _ = self.lex_line(line);
            }
            Some('/') => {
                self.t_vec.push(Token::Law(Op::Div));
                line.next();
                let _ = self.lex_line(line);
            }
            Some('x') => {
                self.t_vec.push(Token::X);
                line.next();
                let _ = self.lex_line(line);
            }
            Some('y') => {
                self.t_vec.push(Token::Y);
                line.next();
                let _ = self.lex_line(line);
            }
            Some('(') => {
                self.t_vec.push(Token::OpenParen);
                line.next();
                let _ = self.lex_line(line);
            }
            Some(')') => {
                self.t_vec.push(Token::CloseParen);
                line.next();
                let _ = self.lex_line(line);
            }
            Some('0'..='9') => {
                self.lex_number(line);
            }
            Some('=') => {
                self.t_vec.push(Token::Eq);
                line.next();
                let _ = self.lex_line(line);
            }
            Some(' ') => {
                line.next();
                let _ = self.lex_line(line);
            }

            Some('#') => {} // a way to leave out a line
            Some(_) => {
                println!("[ERROR] : Unknown char: {:?}", line.peek());
                process::exit(1);
            }
            None => {}
        }
        Ok(())
    }

    fn lex_kw(&mut self, line: &mut Peekable<Chars>) {
        let mut kw = String::new();
        line.next();
        loop {
            match line.peek() {
                Some(c @ 'a'..='z') => {
                    kw.push(*c);
                    line.next();
                }
                Some(c @ 'A'..='Z') => {
                    kw.push(*c);
                    line.next();
                }
                Some(']') => {
                    // found the end of the keyword
                    line.next();
                    break;
                }
                Some(_) => {
                    println!("[ERROR] : Unclosed delimiter '['.");
                    process::exit(1);
                }
                None => {
                    println!("[ERROR] : Unclosed delimiter '['.");
                    process::exit(1);
                }
                
            }
        }

        match kw.as_ref() {
            "Graph" => {
                self.t_vec.push(Token::KW(KeyWord::Graph));
                let _ = self.lex_line(line);
            }
            "End" => {
                self.t_vec.push(Token::KW(KeyWord::End));
                let _ = self.lex_line(line);
            }
            _ => {
                println!("[ERROR] : Unknown keyword : \"{}\".", kw);
            }
            
        }

    }

    fn lex_number(&mut self, line: &mut Peekable<Chars>) {
        let mut buf_int: Vec<i32> = Vec::new();
        let mut buf_dec: Vec<i32> = Vec::new();
        loop {
            match line.peek() {
                Some(x @ '0'..='9') => {
                    buf_int.push(*x as i32 - 48);
                    line.next();
                }
                Some('.') => {
                    // We encountered a decimal number, we now compute the part after before adding it.
                    line.next();
                    loop {
                        match line.peek() {
                            Some(x @ '0'..='9') => {
                                buf_dec.push(*x as i32 - 48);
                                line.next();
                            }
                            Some('.') => {
                                println!("[INFO]: Double '.' in a number, skipping over it.");
                                line.next();
                            }
                            Some(_) => break,
                            None => break,
                        }
                    }
                }

                Some(_) => {
                    let int: i32 = veci32_to_i32(&buf_int);
                    let dec: i32 = veci32_to_i32(&buf_dec);
                    let x: f32 =
                        (int as f32) + ((dec as f32) / 10_i32.pow(buf_dec.len() as u32) as f32);
                    self.t_vec.push(Token::Num(x));

                    let _ = self.lex_line(line);
                    break;
                }
                None => {
                    let int: i32 = veci32_to_i32(&buf_int);
                    let dec: i32 = veci32_to_i32(&buf_dec);
                    let x: f32 =
                        (int as f32) + ((dec as f32) / 10_i32.pow(buf_dec.len() as u32) as f32);
                    self.t_vec.push(Token::Num(x));

                    let _ = self.lex_line(line);

                    break;
                }
            }
        }
    }
}

fn veci32_to_i32(v: &[i32]) -> i32 {
    let mut x: i32 = 0;
    for k in 0..v.len() {
        x += v[k] * 10_i32.pow((v.len() - k - 1) as u32);
    }
    x
}
