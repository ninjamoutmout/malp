mod lexer;
use std::env;
use std::fs::*;
use std::io::*;
use std::process;

use crate::lexer::Lexer;

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    println!("lenn {}", args.len());
    if args.len() != 2 {
        println!("[ERROR] : Too many / few argument have been given.");
        process::exit(1);
    }
    let fp = &args[1];

    let raw = BufReader::new(File::open(fp)?);
    let reader = raw.lines();
    let v = Lexer::lex(reader)?;
    println!("{:?}", v);
    Ok(())
}
